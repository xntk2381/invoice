package main

import (
	"fmt"

	"gitee.com/xntk2381/invoice"
	"gitee.com/xntk2381/invoice/aisino"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()
	a := aisino.New("")
	var info = &invoice.Info{
		Kind:     2,
		Number:   "76863195",
		TypeCode: "021002000404",
	}
	err := a.ZuoFei(info)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("")
	select {}
}
