package main

import (
	"fmt"

	"gitee.com/xntk2381/invoice/aisino"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()
	a := aisino.New("")
	err := a.DaYin(2, "021002000404", "76863195")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("")
	select {}
}
