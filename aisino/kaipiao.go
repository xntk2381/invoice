package aisino

import (
	"fmt"

	"gitee.com/xntk2381/invoice"
	"github.com/go-ole/go-ole/oleutil"
)

// 开票
func (a *AisinoInvoice) KaiPiao(req *invoice.Invoice) (resp *invoice.Info, err error) {
	var (
		retCode int16
		retMsg  string
	)
	err = a.Init()
	if err != nil {
		return
	}
	defer func() {
		a.Release()
	}()

	// 设置发票类别
	fmt.Printf("设置属性 %v：%v\n", "InfoKind", req.Kind)
	a.tax.PutProperty("InfoKind", req.Kind)

	// 读取发票库存
	fmt.Printf("准备调用：%v,读取发票库存\n", "GetInfo")
	_, err = a.tax.CallMethod("GetInfo")
	if err != nil {
		return
	}
	retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "GetInfo", retCode, retMsg)
	if retCode != 3011 {

		err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
		return
	}

	// 设置票面信息
	a.tax.PutProperty("InfoSellerBankAccount", req.SellerBankAccount)
	a.tax.PutProperty("InfoSellerAddressPhone", req.SellerAddressPhone)
	a.tax.PutProperty("InfoClientTaxCode", req.ClientTaxCode)
	a.tax.PutProperty("InfoClientName", req.ClientName)
	a.tax.PutProperty("InfoClientAddressPhone", req.ClientAddressPhone)
	a.tax.PutProperty("InfoInvoicer", req.Invoicer)
	a.tax.PutProperty("InfoChecker", req.Checker)
	a.tax.PutProperty("InfoCashier", req.Cashier)
	a.tax.PutProperty("InfoNotes", req.Notes)

	// 清除明细表全部行
	fmt.Printf("准备调用：%v,清除明细表全部行\n", "ClearInvList")
	_, err = a.tax.CallMethod("ClearInvList")
	if err != nil {
		return
	}
	retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "ClearInvList", retCode, retMsg)
	if retCode != 9000 {

		err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
		return
	}

	for _, item := range req.Items {
		// 初始化明细行
		fmt.Printf("准备调用：%v,初始化明细行\n", "InvListInit")
		_, err = a.tax.CallMethod("InvListInit")
		if err != nil {
			return
		}
		retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
		retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
		fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "InvListInit", retCode, retMsg)
		if retCode != 9000 {

			err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
			return
		}

		a.tax.PutProperty("ListTaxItem", item.TaxItem)
		a.tax.PutProperty("ListStandard", item.Standard)
		a.tax.PutProperty("ListAmount", item.Amount)
		a.tax.PutProperty("ListNumber", item.Number)
		a.tax.PutProperty("ListPrice", item.Price)
		a.tax.PutProperty("InfoTaxRate", item.TaxRate*100) // 9%传 9
		a.tax.PutProperty("ListPriceKind", item.PriceKind)
		a.tax.PutProperty("ListUnit", item.Unit)
		a.tax.PutProperty("ListGoodsName", item.GoodsName)

		var bustr string
		bustr, err = ConvertBatchUpload(item)
		if err != nil {
			return
		}
		fmt.Printf("准备调用：%v\n", "BatchUpload")
		_, err = a.tax.CallMethod("BatchUpload", bustr)
		if err != nil {
			return
		}
		retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
		retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
		fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "BatchUpload", retCode, retMsg)

		// 将上述信息加入到明细列表
		fmt.Printf("准备调用：%v\n", "AddInvList")
		_, err = a.tax.CallMethod("AddInvList")
		if err != nil {
			return
		}
		retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
		retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
		fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "AddInvList", retCode, retMsg)

		if retCode != 9000 {

			err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
			return
		}

	}

	a.tax.PutProperty("CheckEWM", int16(0))

	// 发票开具
	fmt.Printf("准备调用：%v\n", "Invoice")
	_, err = a.tax.CallMethod("Invoice")
	if err != nil {
		return
	}
	retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "Invoice", retCode, retMsg)
	if retCode != 4011 {

		err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
		return
	}

	fph := oleutil.MustGetProperty(a.tax, "InfoNumber").Value().(int32)
	number := fmt.Sprintf("%08d", fph)
	resp = &invoice.Info{
		Kind:     req.Kind,
		Number:   number,                                                          // 发票号
		TypeCode: oleutil.MustGetProperty(a.tax, "InfoTypeCode").Value().(string), // 发票代码
	}

	// 打印发票
	// fmt.Printf("准备调用：%v\n", "PrintInv")
	// _, err = a.tax.CallMethod("PrintInv")
	// if err != nil {
	// 	return
	// }
	// retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	// retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	// fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "PrintInv", retCode, retMsg)
	// if retCode != 5011 {

	// 	err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
	// 	return
	// }
	return
}
