package aisino

import (
	"fmt"

	"github.com/go-ole/go-ole/oleutil"
)

// 打印发票
func (a *AisinoInvoice) DaYin(kind int16, typeCode, number string) (err error) {
	var (
		retCode int16
		retMsg  string
	)
	err = a.Init()
	if err != nil {
		return err
	}
	defer func() {
		a.Release()
	}()

	fmt.Printf("设置属性 %v：%v\n", "InfoKind", kind)
	a.tax.PutProperty("InfoKind", kind)
	fmt.Printf("设置属性 %v：%v\n", "InfoTypeCode", typeCode)
	a.tax.PutProperty("InfoTypeCode", typeCode)
	fmt.Printf("设置属性 %v：%v\n", "InfoNumber", number)
	a.tax.PutProperty("InfoNumber", number)

	// 打印发票
	fmt.Printf("准备调用：%v\n", "PrintInv")
	_, err = a.tax.CallMethod("PrintInv")
	if err != nil {
		return
	}
	retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	fmt.Printf("%v 返回 RetCode=%v RetMsg=%v\n", "PrintInv", retCode, retMsg)
	if retCode != 5011 {
		err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
		return
	}
	return nil
}
