package aisino

import (
	"fmt"

	"gitee.com/xntk2381/invoice"
	"github.com/go-ole/go-ole/oleutil"
)

func (a *AisinoInvoice) ZuoFei(info *invoice.Info) (err error) {
	var (
		retCode int16
		retMsg  string
	)
	err = a.Init()
	defer func() {
		a.Release()
	}()

	// 作废发票
	a.tax.PutProperty("InfoKind", info.Kind)
	a.tax.PutProperty("InfoTypeCode", info.TypeCode)
	a.tax.PutProperty("InfoNumber", info.Number)
	_, err = a.tax.CallMethod("CancelInv")
	if err != nil {
		return
	}
	retCode = oleutil.MustGetProperty(a.tax, "RetCode").Value().(int16)
	retMsg = oleutil.MustGetProperty(a.tax, "RetMsg").Value().(string)
	if retCode != 6011 {
		err = fmt.Errorf("RetCode=%v RetMsg=%v", retCode, retMsg)
		return
	}
	return
}
