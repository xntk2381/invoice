package aisino

import (
	"testing"
)

func TestUtf8ToGbk(t *testing.T) {
	res, err := Utf8ToGbk([]byte("123abc免税"))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(res))
}
