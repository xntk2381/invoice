package invoice

type Info struct {
	Kind     int16  // 0 专票 2 普票 11 货运 12 机动车
	Number   string // 发票号
	TypeCode string // 发票代码
}

// 发票库存
type InvoiceStock struct {
	Number   string      // 发票号
	TypeCode string      // 发票代码
	Stock    interface{} //
	TaxClock interface{}
}

type Invoice struct {
	Kprq     string // 原开票日期，冲票时用
	FaPiaoId int64
	// 电票增加
	Lsh        string //
	Fpzl       string // 电子发票，纸质发票
	ChangJia   string // 厂家 hx
	Kplx       string //0,蓝票，1，红票
	Jshj       string
	Je         string //
	Se         string
	Dzyx       string
	Dlzh       string // 登录名，用于一税号多账户区分用，该登录名为税局页面登录名
	Fjh        string // 分机号，诺诺数电
	Kprid      string // 开票人ID ,诺诺数电
	Sjh        string
	Fphm       string
	Fpdm       string
	Xsf_name   string
	Xsf_nsrsbh string
	Ddrq       string // bwys 订单日期

	Kind               int16  // 0 专票 2 普票 11 货运 12 机动车
	SellerBankAccount  string // 销售方开户行账号
	SellerBank         string // 销售方开户行
	SellerAccount      string // 销售方账号
	SellerAddressPhone string // 销售方地址电话
	SellerAddress      string // 销售方地址电话
	SellerPhone        string // 销售方地址电话
	ClientTaxCode      string // 购买方税号
	ClientName         string // 购买方名称
	ClientAddressPhone string // 购买方地址电话
	ClientBankAccount  string // 购买方开户行账号
	Invoicer           string // 开票人
	Checker            string // 复核人
	Cashier            string // 收款人
	Notes              string // 发票备注
	Sbat               string
	Sct                string
	Items              []InvoiceItem
}

// 发票行信息
type InvoiceItem struct {
	//电票增加
	Fphxz string // 发票行性质， 0：正常行；1：折扣行；2：被折扣行，默认为0
	Se    string // 税额

	TaxItem      string  // 税目
	Standard     string  // 规格型号
	Amount       float64 // 金额
	Number       string  // 数量
	Price        string  // 单价
	TaxRate      float64 // 税率
	PriceKind    int     // 含税标识
	Unit         string  // 单位
	GoodsName    string  // 商品名称
	GoodsNoVer   string  // 编码版本号
	GoodsTaxNo   string  // 税局分类编码
	TaxPre       string  // 是否享受税收优惠政策 0： 不享受 1 享受
	TaxPreCon    string  // 优惠政策内容 免税
	ZeroTax      string  // 零税率标识， 空：非零税率，1：免税 2：不征收 3 普通零税率
	CropGoodsNo  string  // 企业自编码
	TaxDeduction string  // 扣除额
}
